\documentclass{article}
\usepackage[utf8]{inputenc}

\title{APM426 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{January 8 2020}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{mathabx}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{claim}{Claim}

\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\prt}[2]{{\frac{\partial {#1}}{\partial {#2}}}}
\def\ries{{\hat{\mbb{C}}}}
\newcommand{\reals}{\mbb{R}}
\newcommand{\nats}{\mbb{N}}
\newcommand{\ints}{\mbb{Z}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\DeclareMathOperator{\Res}{Res}
\DeclareMathOperator{\Id}{id}

\begin{document}

\maketitle

The reading task for this week is sections 1.1-2.2 from Wald. Today we will be covering the beginnings of general relativity, starting from classical mechanics.

\section*{The Beginnings of General Relativity}

\subsection*{Classical Mechanics}

In classical mechanics, if I ask you to describe a particular event, you will be able to describe it by giving me a time coordinate and 3 space coordinates, as a tuple \((t, x^1, x^2, x^3)\). Note that this also serves as an example of the notation we will be using.

In classical mechanics, these coordinates are well-posed up to a choice of origin, i.e. a zero point \((0, 0, 0)\) and a zero time \(t = 0\). So the only thing you really measure in classical mechanics, i.e. the only thing that is ``gauge invariant" (do not depend on where you set your coordinates), are the quantities \(\Delta t\), \(\Delta x^1\), \(\Delta x^2\), \(\Delta x^3\). So if I give you one event, you cannot give me back its coordinates. But if I give you two different events, you can calculate the distance between them in space and in time, and those quantities will be observer-independent: any two intertial frames flying around the universe, or any two coordinates, will give you back the same interval length for these four quantities.

In particular, there is a notion of ``absolute simultaneity," namely if between two events there is a time difference of zero. This can be thought of as a total ordering on an equivalence class of events: given two events, either they happen at the same time or one happens before the other. It turns out that Einstein had the feeling that there was a contradiction here, because in Newtonian theory, when you have this notion of absolute simultaneity, it means that some problems come up with the speed of light and Maxwell's equations.

Back then, they thought that the speed of light was different for each observer. Maxwell's equations, however, disagreed with this. Also related to classical mechanics, there is this observation by Mach, which is more philosophical, that says that, if we move through the universe at some velocity, typically you see other objects flying past you, and similarly, if you rotate yourself, you see the stars rotating around you. So there is a connection between your rotation and those of the stars. So there must be a way that far-away objects like the stars help us define what velocity and angular momentum \textit{mean} for us, but this is not the case in classical mechanics. This is Mach's principle. So all of these were problems with classical mechanics.

\subsection*{Special Relativity}

In response to this, Einstein devised Special Relativity. Now, instead of a ``3D hyperplane" of simultaneity defined by a given time coordinate, we have, emerging from each event, ``light cones", opening along the positive and negative time axes. Objects in the upper light cone lie in the ``future" of the event, and can be influenced by it. Objects on the upper light cone can only be influenced by light from the event. Similarly, the lower light cone defines ``past": other events are independent.
Curves lying within the light cone are called ``timelike curves", curves lying outside the light cone are called ``spacelike curves." In special relativity, we carry over from classical mechanics the assumption that space can be defined by 4 coordinates. In general relativity, however, we drop the assumption that the universe is ``like" (diffeomorphic to) \(\reals^4\), and instead we only assume that it ``locally" looks like \(\reals^4\). Mathematically, this leads us directly to the notion that the universe is a 4-dimensional manifold. So let's turn a bit to the mathematics of this week.

\subsection*{The Mathematics in Detail}

The first thing we need to cover is the definition of an \(n\)-manifold. Colloquially said, an \(n\)-manifold is a set or topological space that locally ``looks like" \(\reals^n\). So how are we going to implement that?

\begin{definition}[\(n\)-manifold]
An \textbf{\(n\)-dimensional, \(\mc{C}^\infty\) manifold} is a set \(M\) together with a collection of subsets denoted \(\{O_\alpha\}\) such that
\begin{enumerate}

  \item \(M \subset \bigcup_\alpha O_\alpha\)

  \item \(\forall \alpha, \exists \ \text{bijection} \ \psi_\alpha: O_\alpha \to U_\alpha \ \text{open} \ \subset \reals^n\). These are called \textbf{coordinate charts}.

  \item If \(O_\alpha\) and \(O_\beta\) for some \(\alpha, \beta\) overlap, then the map \(\pi_\beta \circ \psi_\alpha^{-1}: \psi_\alpha(O_\alpha \cap O_\beta) \subset U_\alpha \to \psi_\beta(O_\alpha \cap O_\beta) \subset U_\beta\) must be smooth.

\end{enumerate}
\end{definition}
Some examples include:
\begin{enumerate}

  \item \(\reals^n\), with the chart \(\psi = \Id\).

  \item We claim that \(S^3 = \{(x, y, z) \in \reals^3, x^2 + y^2 + z^2 = 1\}\). To prove this, we need to find charts and prove that they satisfy the above conditions. In this case, we can use two stereographic projections.

  Note that, from everyday life, \textit{locally}, the surface of a sphere looks just like \(\reals^2\), i.e. the Earth looks flat, but \textit{globally}, it looks very different, i.e. you can walk around it by going in a straight line.

\end{enumerate}


\end{document}
