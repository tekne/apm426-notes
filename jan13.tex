\documentclass{article}
\usepackage[utf8]{inputenc}

\title{APM426 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{January 13 2020}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{mathabx}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{claim}{Claim}

\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\prt}[2]{{\frac{\partial {#1}}{\partial {#2}}}}
\def\ries{{\hat{\mbb{C}}}}
\newcommand{\reals}{\mbb{R}}
\newcommand{\nats}{\mbb{N}}
\newcommand{\ints}{\mbb{Z}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\DeclareMathOperator{\Res}{Res}
\DeclareMathOperator{\Id}{id}
\DeclareMathOperator{\Ric}{Ric}
\DeclareMathOperator{\Scal}{scal}

\begin{document}

\maketitle

We're going to begin with a little guide, so that we know where we're going. So I'm going to give you a list of keywords, and then we're going to go to the proofs, which we'll discuss, and it'd be nice if some of you could do them. Let's begin.

From the reading, you learned the definition of a \(\mc{C}^\infty\) \(n\)-manifold for \(n \in \nats\), and you learned how to generalize the definition of a derivative from Euclidean space to vector fields. In particular, vector fields map smooth functions on a manifold \(M\) into the real numbers, and they have to satisfy the two properties of being linear and the Leibniz rule.

One thing that is clear from the definition of vector fields is that the set of vector fields at a point \(p \in M\) (i.e. tangent vectors at \(p\)) forms a linear (vector) space (the vector space of vector fields at a point, which themselves can be added, scalar multiplied, etc.), the \textbf{tangent space}. Now this is about as far as the reading went this week. We also learned how to take the commutator of two vectors.

Now, how is this all going to continue and lead us to the equations of general relativity? Let's go through the list:

\section*{The Plan}

\begin{enumerate}

  \item If in linear algebra I give you a vector space of dimension \(n\), what is the first thing that you can do with it? We can get the \textbf{dual} of this vector space: the set of linear maps that ``eat" a vector field and give out a real number, known as the \textbf{cotangent space}. Just as we defined vector fields as ``eating" a function on a manifold and giving a real number back, the dual of this vector space, whose elements are called \textbf{covectors}, eat a vector field at a point and give you back a real number. You can easily show that the dimension of the tangent space at \(p\) is equal to the dimension of the cotangent space at \(p\) (they're both \(n\)). However, with what we know so far, we can't find a canonical isomorphism between the two vector spaces. We can establish one as follows
  \begin{definition}[Basis notation]
  We will write the \(i^{th}\) element of a basis of the tangent space as \(\prt{}{x^i}\) and the \(j^{th}\) element of the corresponding \textbf{dual basis} of the cotangent space as \(dx^j\). In particular, we can write
  \[dx^i\left(\prt{}{x^j}\right) = \delta^i_j\]
  where \(\delta\) denotes the \textbf{Kroenecker delta}
  \[\delta^i_j = \left\{\begin{array}{cr}
    1 & \text{if} \ i = j \\
    0 & \text{otherwise}
  \end{array}\right.\]
  \end{definition}

  It is easy to show that, in this case, \(dx^1,...,dx^n\) forms a basis for the dual space.

  \item Now, more generally, we can start to consider multilinear maps:
  \begin{definition}[Multilinear map]
  A \textbf{multilinear map} \(F\) with \(k\) arguments satisfies the linearity property for each of its arguments: for any scalar \(\alpha\), vector \(v\) and arguments \(a_1,...,a_n\)
  \[F(a_1,...,\alpha a_i + v,...,a_n) = \alpha F(a_1,...,a_n) + F(a_1,...,v,...,a_n)\]
  \end{definition}
  \begin{definition}[Tensor]
  A multilinear map eating tangent vectors and cotangent vectos is called a \textbf{tensor}.
  \end{definition}
  For example, the \textbf{metric} is defined as a tensor which eats two tangent vectos and gives back a real number. For example, in \(\reals^n\), the inner product \(\ip{}{}: \reals^n \times \reals^n \to \reals\) is a tensor. Now, for two more weeks or so, it's going to be definition after definition of differential geometry, but then we can get to general relativity. The goal of all this is to prepare you for that. Don't worry: it's only going to be two weeks.

  \item We will next learn how to define a derivative of tensors on a manifold equipped with a metric. This is called the \textbf{covariant derivative} or, in particular, the \textbf{Levi-Civita connection}.

  \item  Once we have this definition of what a derivative is, similarly to in Riemannian geometry, we can move on to defining \textbf{parallel transport}, which will lead us to \textbf{geodesics}.

  \item We then set up the \textbf{Riemann curvature tensor}, and can state the \textbf{Einstein equations}. At this point we can start looking at questions like ``how much matter has to be in a given area before the curvature becomes so high we get a black hole." One thing I want to underline is that if someone asks you after passing this course what the Einstein equations are, \textbf{do not} reply with
  \[E = mc^2\]
  If you do, I should revoke your grade. They're really
  \[\Ric_{\mu v} - \frac{1}{2}R_{\Scal}g_{\mu v} = T_{\mu v}\]
  On the left hand side, we have the Ricci tensor minus a scalar multiple of the metric of our universe, very beautiful, whereas the left is the stress-energy-momentum tensor. Some people say on the left side we have a very beautiful Greek temple, and on the right we have the ``wood" that we need to carve ourselves.

\end{enumerate}

We'll now take a brief break, and then cover this week's reading

\section*{The Reading}

\subsection*{Manifolds}

Recall the definition of a \(\mc{C}^\infty\) \(n\)-manifold:
\begin{definition}[Manifold]
A set \(M\) is called a \textbf{\(\mc{C}^\infty\) \(n\)-manifold} if
\begin{enumerate}

  \item There exists a collection of sets \(\{\mc{O}_\alpha\}\) such that \(M \subseteq \bigcup_{\alpha}O_\alpha\)

  \item \(\forall \alpha, \exists \psi_\alpha: \mc{O}_\alpha \to U_\alpha \subseteq \reals^n\) bijection. This collection of charts is called an \textbf{atlas}.

  \item If \(\mc{O}_\alpha \cap \mc{O}_\beta \neq \varnothing\), then
  \[\psi_\alpha \circ \psi_\beta^{-1} : \psi_\beta(\mc{O}_\alpha \cap \mc{O}_\beta) \to \psi_\alpha(\mc{O}_\alpha \cap \mc{O}_\beta)\]
  is \(\mc{C}^\infty\).

\end{enumerate}
For this course, we will take \textbf{\(n\)-manifold} to mean \(\mc{C}^\infty\) \(n\)-manifold.
\end{definition}
Note that there are obviously infinitely many equivalent atlases we can define for a given manifold, so when we talk about the atlas, think about the equivalence class! One other question is why we want \(\mc{C}^\infty\): the answer is simply to make things easy. If we definie it with \(\mc{C}^0\) it's called a \textbf{topological manifold}, if we define it with \(\mc{C}^1\) it has what's called a \textbf{differentiable structure}, so we can define a metric on it, but we'll have a problem when we talk about curvature, which is the second derivative. In general, we just pick \(\mc{C}^\infty\) to make our lives easier because (we believe) nature is generic and stable, and \(\mc{C}^k\) is unstable. If we have initial data that leads to a singularity (and, in general, we will deal with these: Schwarschild, for example, has a singularity), we can perturb these special solutions a bit and remove it. So that's why we can make such nice assumptions.

\subsection*{Vectors}

\begin{definition}[Tangent vectors]
Let \(M\) be an \(n\)-manifold.
A \textbf{tangent vector} \(v\) at a point \(p \in M\) is a linear mapping
\(v: \mc{F}(M) \to \reals\) satisfying the \textbf{Leibniz rule}
\[v(fg) = f(p)v(g) + v(f)g(p)\]
where \(\mc{F}(M)\) is the set of smooth functions on \(M\).
Tangent vectors at \(p\) can be added and scalar multiplied pointwise, and hence form a vector space known as the \textbf{tangent space}.
\end{definition}
We claim that the tangent space is finite dimensional, and in fact that
\[\dim(T_pM) = n\]
\begin{proof}
Let \(\psi: O \to U \subset \reals^n\) be a chart with \(p \in O\). Define tangent vectors \(X_\mu: \mc{F} \to \reals\) at \(p\) as follows:
\[\forall f \in \mc{F}, \prt{}{x^\mu} = X_\mu(f) = \left.\prt{}{x_\mu}(f \circ \psi^{-1})\right|_{x = \psi(p)}\]
(this shows two notations: that used in the book and that used everywhere else, including later in the book).
We need to show that they are tangent vectors, and hence are linear and satisfy the Leibniz rule. This is left as an exercise to the reader. We now need to show that the \(X_\mu\)'s are linearly independent. This is equivalent to saying that
\[\sum_\mu\alpha_\mu X_\mu = 0 \iff \alpha_\mu = 0\]
To prove this, we assume that \(\alpha_1,...,\alpha_n\) satisfying
\[\forall f \in \mc{F}, \left(\sum_\mu\alpha_\mu X_\mu\right)(f) = \sum_\mu\alpha_\mu X_\mu(f) = \sum_\mu\alpha_\mu\prt{}{x^\mu}f\]
Note that here I've heavily abused the notation in a way that I will consistently do this semester: I should have written
\[\left.\prt{}{x^\mu}\left(f \circ \psi^{-1}\right)\right|_{x = \psi(p)}\]
but I wrote it in shorthand form \(\prt{}{x^\mu}f\). That's just what is done in GR.
As this holds for all smooth functions on a manifold, in particular, it is true for \(f = x^\mu\) (shorthand for \(\pi_\mu \circ \psi\), another abuse of notation), implying
\[\forall \mu, \alpha_\mu\prt{}{x^\mu}x^\mu = \alpha_i = 0\]
giving the desired result.

So does this show that the dimension is equal to \(n\)? Not quite: it shows that the dimension is \(\geq n\). We now need to show that the \(\prt{}{x^\mu}\)'s span the whole space. Let \(v: \mc{F} \to \reals\) be an arbitrary tangent vector, and let \(f \in \mc{F}\) be smooth around \(p\). We can Taylor expand \(f\) around \(P\) to get
\[f = f(p) + \sum_{\mu = 1}^n(x^\mu - p^\mu)H_\mu(x) \ \text{where} \ H_\mu(p) = \left.\prt{f}{x^\mu}\right|_{x = p}\]
Note that, for a constant \(c\),
\[v(c) = cv(1) = v(1 \cdot c) = cv(1) + 1v(c) \implies v(c) = 0\]
Therefore, we can ignore the constant \(f(p)\) to obtain
\[v(f) = v(f) = v\left(
  \sum_{\mu = 1}^n(x^\mu - p^\mu)H_\mu(x)
\right)
= \sum_{\mu = 1}^n\left(
  v(x^\mu)H_\mu(p) + \cancel{(x^\mu - p^\mu)|_pv(H_\mu(x))}
\right)\]
\[
= \sum_{\mu = 1}^nv(x^\mu)\left.\prt{f}{x^\mu}\right|_p
= \left(\sum_{\mu = 1}^nv(x^\mu)\prt{}{x^\mu}\right)(f)
\implies v = \sum_{p = 1}^nv(x^\mu)\prt{}{x^\mu}
\]
giving the desired result.
\end{proof}
Now we know that every tangent vector can be written as a linear sum of transformations over the coordinate vectors. Now, we can ask the natural question: how do these coefficients change when we change the chart? The way that these tangent vectors transform is important, because later we'll see a tensor transformation law which is just a generalization of this vector transformation law, which is going to be used in some physics applications, specifically that we can pick a paticularly nice coordinate system ata  point.

Let \(\psi\) and \(\psi'\) be two coordinate charts around a point \(p \in M\). Then at \(p\) we have the corresponding bases (plural!) \(\left(\prt{}{x^\mu}\right)_\mu\) and \(\left(\prt{}{x'^{\mu}}\right)_\mu\). A tangent vector \(X\) can hence be expressed as
\[X = \sum_{\mu = 1}^nX^\mu\prt{}{x^\mu} = \sum_{\mu = 1}X'^\mu\prt{}{x'^\mu}\]
Note \(X^\mu, X'^\mu\) are just \textit{numbers}, not the coordinate derivatives in the proof above, which was just notation introduced for pedagogical reasons. So we now have the question: how are \(X^\mu\) and \(X'^\mu\) related? The answer can be reached through multivariable calculus.
Specifically, by the chain rule, we have
\[\prt{}{x^i} = \sum_{j = 1}^n\left(\prt{x'^j}{x^i}\right)\prt{}{x'^j}\]
with the \(\prt{x'^j}{x^i}\) being the coordinates of the Jacobian matrix. Therefore, it holds that
\[X = \sum_{\mu = 1}^n\prt{}{x^\mu} = \sum_
  {\substack{
  \mu \in \{1,...,n\} \\
  \nu \in \{1,...,n\}
  }}
  \left(X^\mu\prt{x^\nu}{x^\mu}\right)\prt{}{x^\nu}
\implies X'\nu = \sum_\mu X^\mu\left(\prt{x'\nu}{x^\mu}\right)\]
So the moral of the story is tangent vectors under coordinate changes transform exactly like a linear basis change under \(\reals^n\). See you all on Wednesday!

\end{document}
