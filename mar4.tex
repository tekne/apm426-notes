\documentclass{article}
\usepackage[utf8]{inputenc}

\title{APM426 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{March 4 2020}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{mathabx}
\usepackage{calc}
\usepackage{accents}
\newcommand{\dbtilde}[1]{\accentset{\approx}{#1}}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{claim}{Claim}

\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\prt}[2]{{\frac{\partial {#1}}{\partial {#2}}}}
\def\ries{{\hat{\mbb{C}}}}
\newcommand{\reals}{\mbb{R}}
\newcommand{\nats}{\mbb{N}}
\newcommand{\ints}{\mbb{Z}}
\newcommand{\gdash}{
  {\hat g} %TODO: make this an actual dash...
}
\newcommand{\lbar}{
  \underline{L}
}
\newcommand{\gammacirc}{
  \gamma^\circ %TODO: the circle should be over the gamma
}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\DeclareMathOperator{\Res}{Res}
\DeclareMathOperator{\Id}{id}
\DeclareMathOperator{\Ric}{Ric}
\DeclareMathOperator{\Scal}{scal}
\DeclareMathOperator{\Tr}{tr}
\DeclareMathOperator{\Area}{area}

\begin{document}

\maketitle

\section*{The Free Conformal Data for the Characteristic IVP of GR}

\subsection*{The Geometric Setup}

Let \(S_0\) be a ... and let \(s\) be the affine parameter \(Ls = 1\), \(s|_{S_0} = 0\). On \(S_s\) we have the induced metric \(\gdash\). On \(S_s\), we define canonical coordinates \((\theta, \phi)\) by transporting along \(L\) (\(L\theta = L\phi = 0\)). We also have, on each \(S_s\), a vector field \(\lbar\) which is
\begin{itemize}

  \item Null

  \item Transversal to \(\mc{H}\) (\(g(L, \lbar) = -1\))

  \item Normal to \(S_s\)

\end{itemize}
There's one more thing I want to do: we have these coordinates \((\theta, \phi)\) on \(S_0\), and in particular I can define myself a metric here, specifically what I call the \textbf{round metric} \(\gammacirc\) on \(S_s \simeq S^2\) by taking
\[\gammacirc = d\theta^2 + \sin^2\theta d\phi^2\]
on \(S_0\) and stipulating \(\mc{L}_L\gammacirc = 0\), i.e. the metric does not change along \(L\). In other words,
\[\gammacirc = d\theta^2 + \sin^2\theta d\phi^2\]
on \underline{all} \(S_s\). Now we can define a scalar function \(\Phi\) such that if I look at the volume element of the induced metric, \(\sqrt{\det \gdash}\), that the following equality should hold:
\[\sqrt{\det \gdash} = \Phi^2\sqrt{\det \gammacirc} \iff \Phi^2 = \frac{\det \gdash}{\det \gammacirc}\]
Note: on the standard sphere of radius \(r\), it turns out that \(\Phi = r\).
The first thing I couild do is to try to calculate \(L(\Phi)\).
\begin{lemma}
  \(L(\Phi^2) = L(\sqrt{\det\gdash}/\sqrt{\det\gammacirc})\), implying that
  \[L\Phi = \frac{\Tr\chi\Phi}{2}, \qquad L(\Phi^2) = \Tr\chi\Phi^2\]
\end{lemma}
\begin{proof}
Write the induced metric \(\gdash\) now as
\[\gdash = \Phi^2\gdash_c \iff \gdash_c = \Phi^{-2}\gdash\]
as \(\gdash_c\) satisfies \(\sqrt{\det\gdash_c} = \sqrt{\det\gammacirc}\).
Let us analyze the transport equation for \(\Phi\). We have
\[L(L(\Phi)) = L\left(\frac{\Tr\chi\Phi}{2}\right)\]
By the Raychaudhuri equation,
\[L\Tr\chi + \frac{1}{2}(\Tr\chi)^2 = -|\hat{x}|^2\]
so we get
\[LL\Phi = \frac{\Phi}{2}\left(
  \cancel{-\frac{1}{2}(\Tr\chi)^2} - |\hat{x}|^2)
  \right) + \cancel{\frac{\Tr\chi}{2}\left(\frac{\Tr\chi\Phi}{2}\right)}
  = -\frac{\phi}{2}|\hat{x}|^2\]
\[\iff LL\Phi + \frac{1}{2}|\hat{x}|^2\Phi = 0\]
This gives us a \underline{linear} \(2^{nd}\) order equation for \(\Phi\).
\begin{lemma}
The quantity \(|\hat{x}|^2\) is conformally \underline{invariant}.
\end{lemma}
\begin{proof}
We arrive at the following setup:
\begin{claim}
Free conformal data is given by specifying the conformal class of \(\gdash\) on \(\mc{H}\), which means you specify the \(\gdash_c\) (freely!) together with (at \(S_0\)) the full metric \(\gdash\), the scalar function \(\Tr\chi\) and the torsion vector \(\zeta\) with scalar function \(\Tr\underline{\chi}\)
\end{claim}
Let's see if we can do this in fifteen minutes. Let's do it in steps:
\begin{enumerate}

  \item Solve for \(\Phi\) on \(\mc{H}\). Because we prescribed \(\gdash\) on \(S_0\) and \(\gdash_c\) on \(\mc{H}\) to have the same conformal part on \(S_0\), the initial value \(\Phi|_{S_0}\) is well-defined.

  Moreover, \(L\Phi = \frac{\Tr\chi\Phi}{2}\) is well-defined. Hence, let \(\Phi\) on \(\mc{H}\) be the solution to the second order DE \(LL\Phi + \frac{1}{2}|\hat{x}|^2\Phi = 0\). We get \(\gdash = \Phi^2\gdash_c\).

  \item Caclulate \(x\) from \(\gdash\) by \(x = \frac{1}{2}\mc{L}_L\gdash\). This gives \(\Tr\chi\) and \(\hat{x}\).

  \item Solve \(\zeta\) on \(\mc{H}\). We have
  ...

\end{enumerate}
\end{proof}
\end{proof}


\end{document}
