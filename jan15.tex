\documentclass{article}
\usepackage[utf8]{inputenc}

\title{APM426 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{January 15 2020}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{mathabx}
\usepackage{calc}
\usepackage{accents}
\newcommand{\dbtilde}[1]{\accentset{\approx}{#1}}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{claim}{Claim}

\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\prt}[2]{{\frac{\partial {#1}}{\partial {#2}}}}
\def\ries{{\hat{\mbb{C}}}}
\newcommand{\reals}{\mbb{R}}
\newcommand{\nats}{\mbb{N}}
\newcommand{\ints}{\mbb{Z}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\DeclareMathOperator{\Res}{Res}
\DeclareMathOperator{\Id}{id}
\DeclareMathOperator{\Ric}{Ric}
\DeclareMathOperator{\Scal}{scal}

\begin{document}

\maketitle

Let \(M\) be an \(n\)-manifold and let \(p \in M\). At \(p\), consider the tangent space \(T_pM\). Let \((v_1,...,v_n)\) be a basis of \(T_pM\) and let \((w_1^*,...,w_n^*)\) be a basis of the dual (cotangent) space \((T_pM)^*\). We can write, for any \(X \in T_pM\), \(W \in T_pM^*\),
\[X = \sum_{\mu = 1}^n X^\mu v_\mu, \qquad W = \sum_{\mu = 1}^n W_\mu w^*_\mu\]
Note we write the indices up on the left but down on the right. This is because they scale differently under change of basis.
\begin{definition}[Tensor]
Let \(T: \left(\prod_{1}^k(T_pM)^*\right) \times \left(\prod_{1}^\ell T_pM\right) \to \reals\) be a multilinear map. Then \(T\) is called a \textbf{\((k, \ell)\)-tensor}.
\end{definition}
We remark that any linear mapping \(\tilde T: T_pM \to \reals\), (i.e. a \((0, 1)\)-tensor) can be written as a cotangent vector. Similarly, any linear mapping \(\dbtilde{T}: (T_pM)^* \to \reals\) (i.e. a \((1, 0)\)-tensor) can be written as a tangent vector \(v\), with \(T(w) = w(v)\). In general, we can also represent a general \((k, \ell)\)-tensor \(T\) in a basis, using the tensor product \(\otimes\).
Define \(v_i \otimes w_j: (T_pM)^* \times (T_pM) \to \reals\) by
\[(\tilde w^*, \tilde v) \mapsto \tilde w^*(v_i) \cdot w_j^*(\tilde v)\]
We can easily check that \(v_i \otimes w_j^*\) is multilinear (and therefore a \((1, 1)\)-tensor) and that
\[\{v_i \otimes w_j^*\}_{i, j = 1,...,n}\]
is a basis of the space of \((1, 1)\) tensors. In general
\begin{definition}[Tensor product]
For a \((k, \ell)\)-tensor \(T\) and a \((k', \ell')\)-tensor \(S\), we define their \textbf{tensor product} to be the \((k + k', \ell + \ell')\)-tensor \(T \otimes S\) given by
\[
  (T \otimes S)(w_1,...,w_{k + k'}, v_1,...,v_{\ell + \ell'})
  = T(w_1,...,w_k, v_1,..., v_\ell)
  \cdot S(w_{k + 1},...,w_{k + k'}, v_{\ell + 1},..., v_{\ell + \ell'})
\]
For any \((k, \ell)\)-tensor \(T\), we can write
\[T = \sum_{\substack{\mu_1,...,\mu_k = 1,...,n \\ \nu_1,...,\nu_\ell = 1,...,n}}
T^{\mu_1,...,\mu_k}_{\nu_1,...,\nu_k}
\partial_{\mu_1} \otimes ... \otimes \partial_{\mu_k} \otimes
dx^{\nu_1} \otimes ... \otimes dx^{\nu_\ell}\]
\end{definition}
For example, let's write our first metric
\[g = \sum_{\mu, \nu = 1}^3g_{\mu\nu}dx^\mu \otimes dx^\nu\]
and define
\[X = \sum_{\mu = 1}^3X^\mu\partial_\mu, \qquad Y = \sum_{\mu = 1}^3Y^\mu\partial_\mu\]
then
\[g(X, Y) = \sum_{\mu, \nu = 1}^3g_{\mu\nu}X^\mu Y^\nu\]
Notice that upper and lower indices are matched when summed over. This leads to the \textbf{Einstein summation convention}, where repeated upper and lower indices are summed over. In this case, the above can be written
\[
g = g_{\mu\nu}dx^\mu \otimes dx^\nu,
\quad X = X^\mu\partial_\mu,
\quad Y = Y^\nu\partial_\nu,
\quad g(X, Y) = g_{\mu\nu}X^\mu Y^\nu\]
In general, we write a \((k, \ell)\)-tensor \(T\) with Latin letters as
\[T^{a_1,...,a_k}_{b_1,...,b_\ell}\]
Note, this doesn't say anything about specific coordinate bases: it just says that we take in \(k\) covectors and \(\ell\) vectors. When we're talking about \(T\) with respect to a particular coordinate system, then we'll use Greek letters
\[T^{\mu_1,...,\mu_k}_{\nu_1,...,\nu_\ell}\]
We can finally now proceed to the following definition:
\begin{definition}[Metric on a manifold]
A \((0, 2)\)-tensor \(g\) (\(g_{ab}\) in the above Latin notation) is a \textbf{metric} if and only if it has
\begin{itemize}

  \item \textbf{Symmetry}: \(\forall X, Y \in T_pM\), \(g(X, Y) = g(Y, X)\).

  \item \textbf{Non-degeneracy}: if \(\forall Y \in T_pM, g(X, Y) = 0\) then \(X = 0\)

\end{itemize}
\end{definition}
One good question: doesn't a metric require non-negativity? The metrics which need positivity are called \textbf{Riemannian metrics}, but in GR we study \textbf{Lorentzian metrics}, which only require this weaker property of non-degeneracy. Let's look at some examples:
\begin{enumerate}

  \item The standard Euclidean metric on \(\reals^3\) can be written
  \[e = \sum_{\mu = 1}^3dx^\mu \otimes dx^\mu = dx^1 \otimes dx^1 + dx^2 \otimes dx^2 + dx^3 \otimes dx^3\]
  We have \(e_{\mu\nu} = 1\) if \(\mu = \nu\), otherwise \(e_{\mu\nu} = 0\), and
  \[e(X, Y) = \sum_{\mu = 1}^3(X, Y) = X \cdot Y\]

  \item Minkowski spacetime \(\reals^4\) has metric
  \[m = -dx^0 \times dx^0 + dx^1 \otimes dx^1 + dx^2 \otimes dx^2 + dx^3 \otimes dx^3\]
  We note that
  \[m(\partial x^0 + \partial x^1, \partial x^0 + \partial x^1) = -1 + 1 = 0\]
  In SR, there are a special class of vectors like this that are orthogonal to themselves, which are called \textbf{null curves}. Nevertheless, \(m\) is not degenerate, because even for this special null vector,
  \[m(\partial x_0 + \partial x_1, \partial x_0 - \partial x_1) = -1 - 1 = -2\]
  Hence, \(m\) is an example of a Lorentzian metric which is not a Riemannian metric.

\end{enumerate}
The reading for this week is 2.2 to 3.3 in Wald.

\end{document}
